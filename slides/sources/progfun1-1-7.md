% Tail Recursion
%
%

Review: Evaluating a Function Application
=========================================

One simple rule : One evaluates a function application $\btt f(e_1, ..., e_n)$

- by evaluating the expressions $\btt e_1, \ldots, e_n$ resulting in the values
$\btt v_1, ..., v_n$, then
- by replacing the application with the body of the function `f`,
in which
- the actual parameters $\btt v_1, ..., v_n$ replace the
formal parameters of `f`.


Application Rewriting Rule
==========================

This can be formalized as a \red{rewriting of the program itself}:

$$
\begin{array}{ll}
  &\btt def\ f (x_1, ..., x_n) = B ;\ ...\ f (v_1, ..., v_n)    \\
 \rightarrow & \\
  &\btt def\ f (x_1, ..., x_n) = B ;\ ...\ [v_1/x_1, ..., v_n/x_n]\,B
\end{array}
$$


Here, $\btt [v_1/x_1, ..., v_n/x_n]\,B$ means:

The expression $\btt B$ in which all occurrences of $\btt x_i$ have been replaced
by $\btt v_i$.

$\btt [v_1/x_1, ..., v_n/x_n]$ is called a \red{substitution}.

Rewriting example:
==================

Consider `gcd`, the function that computes the greatest common divisor of two numbers.

Here's an implementation of `gcd` using Euclid's algorithm.

      def gcd(a: Int, b: Int): Int =
        if b == 0 then a else gcd(b, a % b)

Rewriting example:
==================

`gcd(14, 21)` is evaluated as follows:
\medskip


   `         gcd(14, 21)`
->
   $\rightarrow$ `  if 21 == 0 then 14 else gcd(21, 14 % 21)`
->
   $\rightarrow$ `  if false then 14 else gcd(21, 14 % 21)`
->
   $\rightarrow$ `  gcd(21, 14 % 21)`
->
   $\rightarrow$ `  gcd(21, 14)`
->
   $\rightarrow$ `  if 14 == 0 then 21 else gcd(14, 21 % 14)`
->
   $\rightarrow\dhd$ ` gcd(14, 7)`
->
   $\rightarrow\dhd$ ` gcd(7, 0)`
->
   $\rightarrow$ ` if 0 == 0 then 7 else gcd(0, 7 % 0)`
->
   $\rightarrow$ ` 7`

Another rewriting example:
==========================

Consider `factorial`:

      def factorial(n: Int): Int =
        if n == 0 then 1 else n * factorial(n - 1)
\medskip

   `         factorial(4)`
->
   $\rightarrow$    `   if 4 == 0 then 1 else 4 * factorial(4 - 1)`
3->
   $\rightarrow\dhd$ `  4 * factorial(3)`
->
   $\rightarrow\dhd$ `  4 * (3 * factorial(2))`
->
   $\rightarrow\dhd$ `  4 * (3 * (2 * factorial(1)))`
->
   $\rightarrow\dhd$ `  4 * (3 * (2 * (1 * factorial(0)))`
->
   $\rightarrow\dhd$ `  4 * (3 * (2 * (1 * 1)))`
->
   $\rightarrow\dhd$ `  24`


What are the differences between the two sequences?

Tail Recursion
==============

\red{Implementation Consideration:}
If a function calls itself as its last action, the function's stack frame can be reused. This is called _tail recursion_.

$\Rightarrow$ Tail recursive functions are iterative processes.

In general, if the last action of a function consists of calling a
function (which may be the same), one stack frame would be sufficient for
both functions. Such calls are called _tail-calls_.

Tail Recursion in Scala
=======================

In Scala, only directly recursive calls to the current function are optimized.

One can require that a function is tail-recursive using a `@tailrec` annotation:

      @tailrec
      def gcd(a: Int, b: Int): Int = ...

If the annotation is given, and the implementation of `gcd` were not tail recursive, an error would be issued.


Exercise: Tail recursion
========================

\quiz

Design a tail recursive version of \verb@factorial@.

