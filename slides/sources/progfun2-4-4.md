% Higher-Order Implicits
%
%

Higher-Order Implicits (1)
==========================

Consider how we order two `String` values:

- `"abc" < "abd"`?

\vspace{3cm}

-> We compare the characters of each string, element-wise.

-> **Problem**: How to generalize this process to sequences of any
   element type `A` for which there is an implicit `Ordering[A]`
   instance?

Higher-Order Implicits (2)
==========================

~~~
implicit def listOrdering[A](implicit
  ord: Ordering[A]
): Ordering[List[A]] = ...
~~~

Higher-Order Implicits (3)
==========================

~~~
implicit def listOrdering[A](implicit
  ord: Ordering[A]
): Ordering[List[A]] = { (xs0, ys0) =>
  def loop(xs: List[A], ys: List[A]): Boolean = (xs, ys) match {
    case (x :: xsTail, y :: ysTail) => ord.lt(x, y) && loop(xsTail, ysTail)
    case (xs, ys) => ys.nonEmpty
  }
  loop(xs0, ys0)
}
~~~

~~~
scala> sort(List(List(1, 2, 3), List(1), List(1, 1, 3)))
res0: List[List[Int]] = List(List(1), List(1, 1, 3), List(1, 2, 3))
~~~

Higher-Order Implicits (4)
==========================

~~~
def sort[A](xs: List[A])(implicit ord: Ordering[A]): List[A]
implicit def listOrdering[A](implicit ord: Ordering[A]): Ordering[List[A]]

val xss: List[List[Int]] = ...
sort(xss)
~~~

->

~~~
sort[List[Int]](xss)
~~~

->

~~~
sort[List[Int]](xss)(listOrdering)
~~~

->

~~~
sort[List[Int]](xss)(listOrdering(Ordering.Int))
~~~

Higher-Order Implicits (5)
==========================

An arbitrary number of implicit definitions can be combined
until the search hits a “terminal” definition:

~~~
implicit def a: A = ...
implicit def aToB(implicit a: A): B = ...
implicit def bToC(implicit b: B): C = ...
implicit def cToD(implicit c: C): D = ...

implicitly[D]
~~~

Recursive Implicits
===================

~~~
trait A
implicit def loop(implicit a: A): A = a

implicitly[A]
~~~

->

~~~
          ^
error: diverging implicit expansion for type A
starting with method loop
~~~

Summary
=======

In this lecture, we have seen:

- implicit definitions can also take implicit parameters
- an arbitrary number of implicitly definitions can be chained
  until a terminal definition is reached
