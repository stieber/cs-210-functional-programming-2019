% Pairs and Tuples
%
%
Sorting Lists Faster
====================

As a non-trivial example, let's design a function to sort lists
that is more efficient than insertion sort.

A good algorithm for this is \red{merge sort}. The idea is as
follows:

If the list consists of zero or one elements, it is already sorted.

Otherwise,

 - Separate the list into two sub-lists, each containing around half of
the elements of the original list.
 - Sort the two sub-lists.
 - Merge the two sorted sub-lists into a single sorted list.

First MergeSort Implementation
==============================

Here is the implementation of that algorithm in Scala:

      def msort(xs: List[Int]): List[Int] =
        val n = xs.length / 2
        if n == 0 then xs
        else
          def merge(xs: List[Int], ys: List[Int]) = ???
          val (fst, snd) = xs.splitAt(n)
          merge(msort(fst), msort(snd))

Definition of Merge
===================

Here is a definition of the `merge` function:

      def merge(xs: List[Int], ys: List[Int]) =
        xs match
          case Nil =>
            ys
          case x :: xs1
            ys match
              case Nil =>
                xs
              case y :: ys1 =>
                if x < y then x :: merge(xs1, ys)
                else y :: merge(xs, ys1)
      end merge

The SplitAt Function
====================

The `splitAt` function on lists returns two sublists

 - the elements up the the given index
 - the elements from that index

The lists are returned in a \red{pair}.


Detour: Pair and Tuples
=======================

The pair consisting of `x` and `y` is written `(x, y)` in Scala.

\example

\begin{worksheet}
\verb`  val pair = ("answer", 42)` \wsf pair  : (String, Int) = (answer,42)
\end{worksheet}

The type of `pair` above is `(String, Int)`.

Pairs can also be used as patterns:

\begin{worksheet}
\verb`  val (label, value) = pair` \wsf  label  : String = answer \\
                                 \wsn  value  : Int = 42
\end{worksheet}

This works analogously for tuples with more than two elements.

Exercise
========

The `merge` function as given uses a nested pattern match.

This does not reflect the inherent symmetry of the merge algorithm.

Rewrite `merge` using a pattern matching over pairs.

      def merge(xs: List[Int], ys: List[Int]): List[Int] =
        (xs, ys) match
          ???
